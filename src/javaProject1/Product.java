package javaProject1;

public class Product {
	private String prdNo;
	private String prdName;
	private int prdPrice;
	private int prdYear;
	private String prdMaker;
	
	public Product(String prdNo, String prdName, int prdPrice, int prdYear, String prdMaker) {
		this.prdNo = prdNo;
		this.prdName = prdName;
		this.prdPrice = prdPrice;
		this.prdYear = prdYear;
		this.prdMaker = prdMaker;
	}

	public String getPrdNo() {
		return prdNo;
	}

	public String getPrdName() {
		return prdName;
	}

	public int getPrdPrice() {
		return prdPrice;
	}

	public int getPrdYear() {
		return prdYear;
	}

	public String getPrdMaker() {
		return prdMaker;
	}

	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}

	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	public void setPrdPrice(int prdPrice) {
		this.prdPrice = prdPrice;
	}

	public void setPrdYear(int prdYear) {
		this.prdYear = prdYear;
	}

	public void setPrdMaker(String prdMaker) {
		this.prdMaker = prdMaker;
	}
	
	@Override
	public String toString() {
		return String.format("%s\t%s\t%d\t%d\t%s",
				prdNo, prdName, prdPrice, prdYear, prdMaker);
	}
}
